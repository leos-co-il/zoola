<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="drop-menu">
		<nav class="drop-nav">
			<?php getMenu('header-menu', '2', '', ''); ?>
		</nav>
	</div>
    <div class="container-fluid">
        <div class="row justify-content-between">
			<div class="col d-flex justify-content-start align-items-center p-col-0">
				<button class="hamburger hamburger--spin menu-trigger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
				<a href="<?= wc_get_cart_url(); ?>" class="header-btn">
					<img src="<?= ICONS ?>basket.png" alt="cart">
					<span class="count-circle" id="cart-count">
						<?= WC()->cart->get_cart_contents_count(); ?>
					</span>
				</a>
				<div class="search-trigger">
					<img src="<?= ICONS ?>search.png" alt="search-trigger">
				</div>
			</div>
			<?php if ($logo = opt('logo')) : ?>
				<div class="col-md-auto col-sm-3 col-4 d-flex justify-content-center align-items-center">
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				</div>
			<?php endif; ?>
			<div class="col d-flex justify-content-end align-items-center p-col-0">
				<?php if ($tel = opt('tel')) : ?>
					<a href="tel:<?= $tel; ?>" class="header-tel">
						<span class="tel-number"><?= $tel; ?></span>
						<img src="<?= ICONS ?>header-tel.png" alt="phone">
					</a>
				<?php endif;
				if ($mail = opt('mail')) : ?>
					<a href="mailto:<?= $mail; ?>" class="header-mail">
						<img src="<?= ICONS ?>header-mail.png" alt="mail">
					</a>
				<?php endif; ?>
			</div>
        </div>
    </div>
</header>
