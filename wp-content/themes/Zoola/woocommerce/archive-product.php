<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;
global $wp_query;
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$max_pages = $wp_query->max_num_pages;
if (is_shop()) {
	$query = wc_get_page_id('shop');
	$image = has_post_thumbnail($query) ? postThumb($query) : '';
} else {
	$query_curr = get_queried_object();
	$query = $query_curr;
	$thumbnail_id = get_term_meta( $query->term_id, 'thumbnail_id', true );
	$image = wp_get_attachment_url( $thumbnail_id );
}
get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>
	<section class="top-archive" style="background-image: url('<?= $image; ?>')">
		<div class="archive-overlay-back">
			<span class="vertical-text">Happy</span>
		</div>
		<div class="title-wrap">
			<div class="container">
				<div class="row justify-content-center align-items-center">
					<div class="col-auto">
						<h1 class="base-title shop-main-title"><?php woocommerce_page_title(); ?></h1>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="base-output text-center">
							<?php
							/**
							 * Hook: woocommerce_archive_description.
							 *
							 * @hooked woocommerce_taxonomy_archive_description - 10
							 * @hooked woocommerce_product_archive_description - 10
							 */
							do_action( 'woocommerce_archive_description' );
							?>
						</div>
					</div>
				</div>
				<div class="row justify-content-start">
					<div class="col-auto">
						<?php do_action('woocommerce_before_shop_loop'); ?>
					</div>
				</div>
			</div>
		</div>
<!--		<div class="container-fluid">-->
<!--			<div class="row justify-content-center">-->
<!--				<div class="col-xl-11 col-12">-->
<!--					<div class="breadcrumbs-custom">-->
<!--						--><?php
//						woocommerce_breadcrumb([
//								'delimiter' => '>',
//						]);
//						?>
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
	</section>
	<div class="container cont-prods-top mb-4">
		<div class="row">
			<div class="col-12">
				<div class="woo-notice">
					<?php woocommerce_output_all_notices() ?>
				</div>
				<?php


				if ( woocommerce_product_loop() ) {
					echo '<div class="row justify-content-center align-items-stretch put-here-prods">';
					if ( wc_get_loop_prop( 'total' ) ) {
							while ( have_posts() ) {
								the_post();
								/**
								 * Hook: woocommerce_shop_loop.
								 */
								do_action( 'woocommerce_shop_loop' );
								get_template_part('views/partials/card', 'product_col',
										[
												'product' => get_the_ID(),
										]);
							}
						}
						wp_reset_postdata();
					echo '<div>';
				} else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				}
				?>
				<?php if( $paged < $max_pages ) : ?>
					<div class="row justify-content-center mt-4 mb-5">
						<div class="col-auto">
							<div id="loadmore" data-load="טען עוד" data-loading="טוען">
								<a href="#" data-maxpages="<?= $max_pages ?>" data-paged="<?= $paged ?>"
								   class="more-link">
									טען עוד
								</a>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );
if ($reviews = get_field('review_item', $query)) {
	get_template_part('views/partials/content', 'reviews', [
			'img' => get_field('reviews_img', $query),
			'reviews' => $reviews
	]);
}
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => get_field('slider_img', $query),
					'content' => $slider,
					'experience' => true
			]);
}
get_footer( 'shop' );
