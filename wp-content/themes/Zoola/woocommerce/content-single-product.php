<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$post_id = $product->get_id();
$fields = get_fields();
$category = get_the_terms($post_id, 'product_cat');
$ids = [];
if ($category) {
	foreach ($category as $cat_id) {
		$ids[] = $cat_id->term_id;
	}
}
$post_link = get_the_permalink();
$product_thumb = wp_get_attachment_image_url( get_post_thumbnail_id($post_id), 'full' );;
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
	foreach ($product_gallery as $_item){
		$product_gallery_images[] = wp_get_attachment_image_url($_item, 'large', '');
	}
}
$acc_title = $fields['acc_title'];
$info_del = $fields['acc_delivery'] ?? opt('prod_delivery');
$info_returns = $fields['acc_returns'] ?? opt('acc_title');
$info_content = get_the_content();
$related_products = get_posts([
		'post_type' => 'product',
		'exclude' => $post_id,
		'numberposts' => 4,
		'tax_query' => [
				[
						'taxonomy' => 'product_cat',
						'field' => 'term_id',
						'terms' => $ids,
				],
		],
]);
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'page-body', $product ); ?>>
	<div class="container product-body">
		<div class="row">
			<div class="col-12 hidden-title">
				<h2 class="contact-form-title text-right"><?php the_title(); ?></h2>
			</div>
			<div class="col-lg-6 gallery-slider-wrap">
				<div class="gallery-slider" dir="rtl">
					<?php if($product_thumb): ?>
						<div class="p-2">
							<a class="big-slider-item" style="background-image: url('<?= $product_thumb; ?>')"
							   href="<?= $product_thumb; ?>" data-lightbox="images"></a>
						</div>
					<?php endif;
					foreach ($product_gallery_images as $img): ?>
						<div class="p-2">
							<a class="big-slider-item" style="background-image: url('<?= $img; ?>')"
							   href="<?= $img; ?>" data-lightbox="images">
							</a>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="thumbs" dir="rtl">
					<?php if($product_thumb): ?>
						<div class="p-2">
							<a class="thumb-item" style="background-image: url('<?= $product_thumb; ?>')"
							   href="<?= $product_thumb; ?>" data-lightbox="images-small"></a>
						</div>
					<?php endif;
					foreach ($product_gallery_images as $img): ?>
						<div class="p-2">
							<a class="thumb-item" style="background-image: url('<?= $img; ?>')"
							   href="<?= $img; ?>" data-lightbox="images-small">
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="summary entry-summary">
					<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' ); ?>
					<div class="params-wrapper">
						<?php if ($fields['params']) : ?>
							<div class="params-item">
								<img src="<?= ICONS ?>prod-size.png" alt="size-icon">
								<h3 class="param-title"><?= $fields['params']; ?></h3>
							</div>
						<?php endif;
						if ($fields['delivery']) : ?>
							<div class="params-item">
								<img src="<?= ICONS ?>prod-delivery.png" alt="delivery-icon">
								<h3 class="param-title"><?= $fields['delivery']; ?></h3>
							</div>
						<?php endif;
						if ($fields['delivery_time']) : ?>
							<div class="params-item">
								<img src="<?= ICONS ?>prod-time.png" alt="delivery_time-icon">
								<h3 class="param-title"><?= $fields['delivery_time']; ?></h3>
							</div>
						<?php endif; ?>
					</div>
					<?php do_action('display_excerpt_custom');
					if ($info_returns || $info_del || $info_content): ?>
						<div class="product-item-info">
							<?php if ($acc_title) : ?>
								<h3 class="accordion-title"><?= $acc_title; ?></h3>
							<?php endif; ?>
							<div id="accordion-product">
								<?php if ($info_content) : $c = 1; ?>
									<div class="card">
										<div class="card-header" id="heading<?= $c ?>">
											<h5 class="mb-0 accordion-button-wrap">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $c ?>" aria-expanded="true" aria-controls="collapse<?= $c ?>">
													תיאור
												</button>
											</h5>
										</div>
										<div id="collapse<?= $c ?>" class="collapse" aria-labelledby="heading<?= $c ?>" data-parent="#accordion-product">
											<div class="card-body base-output slider-output">
												<?= $info_content; ?>
											</div>
										</div>
									</div>
								<?php endif;
								if ($info_del) : $x = 2; ?>
									<div class="card">
										<div class="card-header" id="heading<?= $x ?>">
											<h5 class="mb-0 accordion-button-wrap">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $x ?>" aria-expanded="true" aria-controls="collapse<?= $x ?>">
													משלוחים
												</button>
											</h5>
										</div>
										<div id="collapse<?= $x ?>" class="collapse" aria-labelledby="heading<?= $x ?>" data-parent="#accordion-product">
											<div class="card-body base-output slider-output">
												<?= $info_del; ?>
											</div>
										</div>
									</div>
								<?php endif;
								if ($info_returns) : $x = 3; ?>
									<div class="card">
										<div class="card-header" id="heading<?= $x ?>">
											<h5 class="mb-0 accordion-button-wrap">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $x ?>" aria-expanded="true" aria-controls="collapse<?= $x ?>">
													החזרות
												</button>
											</h5>
										</div>
										<div id="collapse<?= $x ?>" class="collapse" aria-labelledby="heading<?= $x ?>" data-parent="#accordion-product">
											<div class="card-body base-output slider-output">
												<?= $info_returns; ?>
											</div>
										</div>
									</div>
								<?php endif;?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="product-share-line">
		<div class="trigger-wrap">
			<a class="social-trigger">
				<span class="social-item-text">
					שתפו את המאמר הזה
				</span>
				<span class="social-item">
					<i class="fas fa-share-alt"></i>
				</span>
			</a>
			<div class="all-socials item-socials" id="show-socials">
				<a class="socials-wrap social-item" href="mailto:?&subject=&body=<?= $post_link; ?>">
					<i class="fas fa-envelope"></i>
				</a>
				<a class="socials-wrap social-item" href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>"
				   target="_blank">
					<i class="fab fa-facebook-f"></i>
				</a>
				<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $post_link; ?>" target="_blank"
				   class="socials-wrap social-item">
					<i class="fab fa-whatsapp"></i>
				</a>
				<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= $post_link; ?>&title=&summary=&source="
				   target="_blank"
				   class="socials-wrap social-item">
					<i class="fab fa-linkedin-in"></i>
				</a>
			</div>
		</div>
	</div>
	<div class="add-cart-line sticky mb-4">
		<?php if ( ! $product->is_purchasable() ) {
			return;
		}
		echo wc_get_stock_html( $product ); // WPCS: XSS ok.

		if ( $product->is_in_stock() ) : ?>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="add-to-cart-single-item">
							<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

							<form class="cart row" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>"
								  method="post" enctype='multipart/form-data'>
								<div class="col-lg-6 col-12 qty-col-wrap">
									<div class="wrapper-product-wish">
										<div class="qty-wrap">
											<div class="plus" data-id="<?= $product->get_id() ?>"> + </div>
											<?php

											do_action( 'woocommerce_before_add_to_cart_quantity' );

											woocommerce_quantity_input(
													array(
															'classes' => 'qty-for-' .  $product->get_id(),
															'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
															'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
															'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
													)
											);

											do_action( 'woocommerce_after_add_to_cart_quantity' );

											?>
											<div class="minus" data-id="<?= $product->get_id() ?>"> - </div>
										</div>
									</div>
									<div class="price-total-wrap">
										<?php $sym = get_woocommerce_currency_symbol(); ?>
										<span>סה”כ</span>
										<div class="price-final-sale final-price-title ml-3" data-price_sale="<?= $product->get_sale_price(); ?>" data-sym="<?= $sym; ?>">
											<?= $sym.$product->get_sale_price(); ?>
										</div>
										<div class="price-final final-price-title" data-price="<?= $product->get_regular_price(); ?>" data-sym="<?= $sym; ?>">
											<?= $sym.$product->get_regular_price(); ?>
										</div>
									</div>
								</div>
								<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
								<div class="buy-buttons col-lg-6 col-12">
									<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="add-custom single_add_to_cart_button button alt">
										<span>הוספה לסל</span>
									</button>
									<a class="buy-now-custom buy-now-style" data-url="<?= site_url(); ?>"
									   data-id="<?= $id = $product->get_id(); ?>" href="<?= site_url(); ?>/checkout/?add-to-cart=<?= $id; ?>&quantity=1'">
										קניה מהירה
									</a>
								</div>
								<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
							</form>

							<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<div class="container">
		<div class="row">
			<div class="col">
				<?php
				/**
				 * Hook: woocommerce_after_single_product_summary.
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary' );
				?>
			</div>
		</div>
	</div>
</div>
<?php
get_template_part('views/partials/content', 'reviews', [
		'img' => $fields['reviews_img'],
		'reviews' => $fields['review_item']
]);
if ($related_products) : ?>
	<div class="products-output same-cat-prods mb-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto mb-4">
					<h2 class="block-title">המוצרים שיענינו אתכם</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($related_products as $product) : ?>
					<div class="col-lg-3 col-md-6 col-sm-11 col-12 mb-3">
						<?php setup_postdata($GLOBALS['post'] =& $product);
						wc_get_template_part( 'content', 'product' ); ?>
					</div>
				<?php endforeach; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['slider_img'],
					'content' => $fields['single_slider_seo'],
					'experience' => false
			]);
}
do_action( 'woocommerce_after_single_product' ); ?>
