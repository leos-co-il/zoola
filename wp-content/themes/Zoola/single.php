<?php

the_post();
get_header();
$fields = get_fields();
$current_id = get_the_permalink();
?>
<article class="page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-7 col-12 d-flex flex-column align-items-start">
				<div class="base-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
				<div class="trigger-wrap mb-3">
					<a class="social-trigger">
						<span class="social-item-text">
							שתפו את המאמר הזה
						</span>
						<span class="social-item">
							<i class="fas fa-share-alt"></i>
						</span>
					</a>
					<div class="all-socials item-socials" id="show-socials">
						<a class="socials-wrap social-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<i class="fas fa-envelope"></i>
						</a>
						<a class="socials-wrap social-item" href="https://www.facebook.com/sharer/sharer.php?u=#<?= $current_id; ?>"
						   target="_blank">
							<i class="fab fa-facebook-f"></i>
						</a>
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="socials-wrap social-item">
							<i class="fab fa-whatsapp"></i>
						</a>
						<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= $current_id; ?>&title=&summary=&source="
						   target="_blank"
						   class="socials-wrap social-item">
							<i class="fab fa-linkedin-in"></i>
						</a>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-5 col-12">
				<?php if (has_post_thumbnail()) : ?>
					<img src="<?= postThumb(); ?>" alt="post-image" class="w-100 my-lg-0 my-3">
				<?php endif; ?>
			</div>
		</div>
		<?php if ($fields['post_video_img'] && ($fields['post_video_img']['0']['acf_fc_layout'] === 'post_video')
		&& (isset($fields['post_video_img']['0']['video_url']))) : ?>
			<div class="row">
				<div class="col-12">
					<div class="video-back-violet">
						<div class="video-item" style="background-image: url('<?= getYoutubeThumb($fields['post_video_img']['0']['video_url']); ?>')">
							<div class="put-video-here"></div>
							<span class="play-button-post" data-id="<?= getYoutubeId($fields['post_video_img']['0']['video_url']); ?>">
								<img src="<?= ICONS ?>play.png" alt="play-button">
							</span>
						</div>
					</div>
				</div>
			</div>
		<?php elseif ($fields['post_video_img'] && ($fields['post_video_img']['0']['acf_fc_layout'] === 'post_img')
				&& (isset($fields['post_video_img']['0']['img_url']))) : ?>
			<div class="row">
				<div class="col-12">
					<img src="<?= $fields['post_video_img']['0']['img_url']['url']; ?>" alt="post-img-2">
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>

<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 3,
		'post_type' => 'post',
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
}
if ($samePosts) {
	get_template_part('views/partials/content', 'posts_output',
			[
					'title' => $fields['same_title'] ? $fields['same_title'] : 'מאמרים נוספים',
					'text' => $fields['same_text'],
					'posts' => $samePosts,
			]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['slider_img'],
					'content' => $fields['single_slider_seo'],
					'experience' => false
			]);
}
get_footer(); ?>
