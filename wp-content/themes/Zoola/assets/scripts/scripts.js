(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	// function addToCartAnimation(button){
	// 	var target        = $('#mini-cart'),
	// 		target_offset = target.offset();
	//
	// 	var target_x = target_offset.left,
	// 		target_y = target_offset.top;
	//
	// 	var obj_id = 1 + Math.floor(Math.random() * 100000),
	// 		obj_class = 'cart-animation-helper',
	// 		obj_class_id = obj_class + '_' + obj_id;
	//
	// 	var obj = $("<div>", {
	// 		'class': obj_class + ' ' + obj_class_id
	// 	});
	//
	// 	button.parent().parent().append(obj);
	//
	// 	var obj_offset = obj.offset(),
	// 		dist_x = target_x - obj_offset.left + 10,
	// 		dist_y = target_y - obj_offset.top + 10,
	// 		delay  = 0.8; // seconds
	//
	// 	setTimeout(function(){
	// 		obj.css({
	// 			'transition': 'transform ' + delay + 's ease-in',
	// 			'transform' : "translateX(" + dist_x + "px)"
	// 		});
	// 		$('<style>.' + obj_class_id + ':after{ \
	// 			transform: translateY(' + dist_y + 'px); \
	// 			opacity: 1; \
	// 			z-index: 99999999999; \
	// 			border-radius: 100%; \
	// 			height: 20px; \
	// 			width: 20px; margin: 0; \
	// 		}</style>').appendTo('head');
	// 	}, 0);
	//
	//
	// 	obj.show(1).delay((delay + 0.02) * 1000).hide(1, function() {
	// 		$(obj).remove();
	// 	});
	// }
	function update_cart_count(){
		var count = $('#cart-count');
		count.html('<i class="far fa-smile-beam fa-spin" style="color: #fff"></i>');

		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'POST',
			data: {
				action: 'update_cart_count',
			},
			success: function (results) {
				count.html(results/10);
			}
		});

	}
	$( document ).ready(function() {
		// $('.single_add_to_cart_button').click(function (e) {
		// 	e.preventDefault();
		// 	addToCartAnimation($(this));
		// 	var cartEl = $('.mini-cart-wrap');
		// 	cartEl.html('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');
		//
		//
		// 	var btn = $(this);
		// 	btn.addClass('loading');
		//
		//
		// 	$(this).addClass('adding-cart');
		// 	var product_id = $(this).data('id');
		// 	var quantity = $('.qty-for-' + product_id).val();
		//
		//
		// 	jQuery.ajax({
		// 		url: '/wp-admin/admin-ajax.php',
		// 		type: 'POST',
		// 		data: {
		// 			action: 'add_product_to_cart',
		// 			product_id: product_id,
		// 			quantity: quantity,
		// 		},
		//
		// 		success: function (results) {
		// 			btn.removeClass('loading').addClass('clicked');
		// 			cartEl.html(results);
		// 			update_cart_count();
		// 		}
		// 	});
		//
		// });
		function intToFloat(num, decPlaces) { return num.toFixed(decPlaces); }

		$(document).on('click', '.plus, .minus', function () {

			var $_class = $(this).hasClass('mini-cart-ctrl') ? '.mini-cart-qty-for-' : '.qty-for-';
			var qty =  $($_class + $(this).data('id'));
			var val = parseFloat(qty.val());
			var max = 9999;
			var min = 1;
			var step = 1;
			if ($(this).is('.plus')) {
				if (max && (max <= val)) {
					qty.val(max);
				} else {
					qty.val(val + step);
				}
			} else {
				if (min && (min >= val)) {
					qty.val(min);
				} else if (val > 1) {
					qty.val(val - step);
				}
			}
			var ret = qty.val();
			var sale = $('.price-final-sale');
			var priceSale = sale.data('price_sale');
			var final = $('.price-final');
			var symbol = final.data('sym');
			var priceFinal = final.data('price');
			var priceReg = intToFloat(ret * priceFinal, 2);
			var priceSaleTotal = intToFloat(ret * priceSale, 2);
			sale.html(priceSaleTotal+symbol);
			final.html(priceReg+symbol);

			var buyLink = $('.buy-now-custom');
			var siteUrl = buyLink.data('url');
			var prodId = buyLink.data('id');
			var finalLink = siteUrl+'/checkout/?add-to-cart='+prodId+'&quantity='+qty.val();
			buyLink.attr('href', finalLink);
			console.log(buyLink);
			if($(this).hasClass('mini-cart-ctrl')){
				$('.preloader').addClass('now-loading');
				updateMiniCart($(this).data('key'), qty.val());
			}

		});
		// $('.input-count').on('change keyup paste', function() {
		// 	var ret = parseInt($('#input-count-1').val()) * parseInt($('#input-count-2').val() || '0');
		// 	var sale = $('.price-final-sale');
		// 	var priceSale = sale.data('price_sale');
		// 	var final = $('.price-final');
		// 	var priceFinal = final.data('price');
		// 	var priceReg = intToFloat(ret * priceFinal, 2);
		// 	var priceSaleTotal = intToFloat(ret * priceSale, 2);
		// 	$('.qty').val(ret);
		// 	sale.val(priceSaleTotal);
		// 	sale.html(priceSaleTotal);
		// 	final.val(priceReg);
		// 	final.html(priceReg);
		// 	var buyLink = $('.buy-now-custom');
		// 	var siteUrl = buyLink.data('url');
		// 	var prodId = buyLink.data('id');
		// 	var finalLink = siteUrl+'/checkout/?add-to-cart='+prodId+'&quantity='+ret;
		// 	buyLink.attr('href', finalLink);
		// 	console.log(buyLink);
		// });

		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.cat-item-img').hover(function (){
			$(this).children('.cat-item-info').children('.cat-item-info-part').slideFadeToggle();
		});
		$('.search-trigger').click(function () {
			$('.pop-search').addClass('show-search');
			$('.float-search').addClass('show-float-search');
		});
		$('.close-search').click(function () {
			$('.pop-search').removeClass('show-search');
			$('.float-search').removeClass('show-float-search');
		});
		$('.pop-trigger').click(function () {
			$('.float-form').addClass('show-form');
			$('.pop-body').addClass('body-hidden');
		});
		$('.close-pop').click(function () {
			$('.float-form').removeClass('show-form');
			$('.pop-body').removeClass('body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: true,
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			rtl: true,
			asNavFor: '.thumbs'
		});
		$('.thumbs').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			asNavFor: '.gallery-slider',
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				// {
				// 	breakpoint: 768,
				// 	settings: {
				// 		slidesToShow: 2,
				// 	}
				// },
			]
		});
		$('.product-slider').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				// {
				// 	breakpoint: 992,
				// 	settings: {
				// 		slidesToShow: 2,
				// 	}
				// },
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 500,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$('.play-button-post').click(function() {
			var id = $(this).data('id');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').addClass('active-faq');
			show.parent().children('.question-title').children('.arrow-top').addClass('hide');
			show.parent().children('.question-title').children('.arrow-bottom').addClass('show');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').removeClass('active-faq');
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('hide');
			collapsed.parent().children('.question-title').children('.arrow-bottom').removeClass('show');
		});
		var prodAcc = $('#accordion-product');
		prodAcc.on('shown.bs.collapse', function (e) {
			$('#accordion-product .collapse.show').parent().addClass('active');
		});

		prodAcc.on('hide.bs.collapse', function (e) {
			$('#accordion-product .collapse.show').parent().removeClass('active');
		});
		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
	});

	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		// var params = $('.take-json').html();
		var ids = '';
		var video = $(this).data('content');
		var page = $(this).data('page');

		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				// postType: postType,
				termID: termID,
				ids: ids,
				page: page,
				video: video,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
	//More products
	var button = $( '#loadmore a' );
	var paged = button.data( 'paged' );
	var	maxPages = button.data( 'maxpages' );
	var textLoad = button.data('loading');
	var textLoadMore = button.data('load');

	button.click( function( event ) {

		event.preventDefault();
		var ids = '';
		$('.more-prod').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		$.ajax({
			type : 'POST',
			dataType: 'json',
			url: '/wp-admin/admin-ajax.php',
			data : {
				paged : paged,
				ids: ids,
				action : 'loadmore'
			},
			beforeSend : function() {
				button.text(textLoad);
			},
			success : function( data ){

				paged++;
				$('.put-here-prods').append(data.html);
				button.text(textLoadMore);

				if( paged === maxPages ) {
					button.remove();
				}
				if (!data.html) {
					$('.more-link').addClass('hide');
				}
			}

		});

	} );
})( jQuery );
