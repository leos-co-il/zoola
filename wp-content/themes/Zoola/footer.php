<?php

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$open_hours = opt('open_hours');
?>
<footer>
	<div class="foo-form-block">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-9 col-md-10 col-11">
					<div class="row justify-content-center">
						<div class="form-wrapper-inside">
							<?php if ($f_main_title = opt('foo_main_title')) : ?>
								<h2 class="foo-main-title"><?= $f_main_title; ?></h2>
							<?php endif;
							if ($f_subtitle = opt('foo_form_title')) : ?>
								<h3 class="foo-form-title"><?= $f_subtitle; ?></h3>
							<?php endif; ?>
							<?php getForm('9'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png">
			<h4 class="to-top-text mt-2">למעלה</h4>
		</a>
		<div class="container footer-container-menu">
			<div class="row justify-content-sm-between justify-content-center align-items-start">
				<div class="col-xl-3 col-md-auto col-sm-6 col-10 foo-menu">
					<h3 class="foo-title">
						תפריט ראשי
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '2'); ?>
					</div>
				</div>
				<div class="col-md col-10 foo-menu foo-links-menu">
					<h3 class="foo-title">
						<?php $foo_l_title = opt('foo_menu_links_title');
						echo $foo_l_title ? $foo_l_title : 'קישורים'; ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '2', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<div class="col-md-auto col-sm-6 col-10 foo-menu contacts-footer-menu">
					<h3 class="foo-title">
						פרטי התקשרות
					</h3>
					<div class="menu-border-top">
						<ul class="contact-list d-flex flex-column">
							<?php if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info-footer">
										<?= $tel; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<?= $mail; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($address) : ?>
								<li>
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-footer" target="_blank">
										<?= $address; ?>
									</a>
								</li>
							<?php endif;
							if ($open_hours) : ?>
								<li>
									<span class="contact-info-footer" target="_blank">
										<?= $open_hours; ?>
									</span>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>


<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
