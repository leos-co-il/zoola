<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$open_hours = opt('open_hours');
?>
<article class="page-body contact-page-body">
	<div class="contact-page-back">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h1 class="block-title"><?php the_title(); ?></h1>
					<div class="base-output slider-output about-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<div class="col-lg-6 col-md-10 col-11 contact-col">
					<?php if ($tel) : ?>
						<a href="tel:<?= $tel; ?>" class="contact-item wow flipInX" data-wow-delay="0.2s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-tel.png">
							</div>
							<div class="contact-info">
								<h3 class="contact-title">חייגו אלינו</h3>
								<span class="contact-type"><?= $tel; ?></span>
							</div>
						</a>
					<?php endif; ?>
					<?php if ($mail) : ?>
						<a href="mailto:<?= $mail; ?>" class="contact-item wow flipInX" data-wow-delay="0.4s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-mail.png">
							</div>
							<div class="contact-info">
								<h3 class="contact-title">כיתבו לנו</h3>
								<span class="contact-type"><?= $mail; ?></span>
							</div>
						</a>
					<?php endif;
					if ($address) : ?>
						<a href="https://waze.com/ul?q=<?= $address; ?>" class="contact-item wow flipInX" data-wow-delay="0.6s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-geo.png">
							</div>
							<div class="contact-info">
								<span class="contact-title">בקרו אותנו</span>
								<h3 class="contact-type"><?= $address; ?></h3>
							</div>
						</a>
					<?php endif;
					if ($open_hours) : ?>
						<div  class="contact-item wow flipInX" data-wow-delay="0.8s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-hours.png">
							</div>
							<div class="contact-info">
								<h3 class="contact-title">השעות שלנו</h3>
								<span class="contact-type"><?= $address; ?></span>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-lg-6 col-12">
					<div class="contact-form-block">
						<?php if ($fields['contact_form_title']) : ?>
							<h2 class="contact-form-title"><?= $fields['contact_form_title']; ?></h2>
						<?php endif;
						getForm('6'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($map = opt('map_image')) : ?>
		<a href="<?= $map['url']; ?>" data-lightbox="map" class="w-100 map-big">
			<img src="<?= $map['url']; ?>" alt="map" class="w-100">
		</a>
		<a class="map-mob" style="background-image: url('<?= $map['url']; ?>')" href="<?= $map['url']; ?>"
		   data-lightbox="map"></a>
	<?php endif; ?>
</article>
<?php if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'block_text' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
