<?php if (isset($args['content']) && ($args['content'])) :
	$slider_img = isset($args['img']) ? ($args['img']) : '';
$exp = (isset($args['experience']) && $args['experience']); ?>
	<div class="base-slider-block arrows-slider arrows-slider-base">
		<div class="container">
			<div class="row justify-content-between align-items-center <?= $exp ? 'flex-row-reverse' : ''; ?>">
				<?php if ($slider_img) : ?>
					<div class="col-lg-6 col-12 slider-img-col" <?php if ($exp) : ?>
						style="background-image: url('<?= IMG ?>slider-back.png'); padding: 50px 15px;"
					<?php endif; ?>>
						<img src="<?= $slider_img['url']; ?>" class="img-slider">
						<?php if ($exp) : ?>
							<img src="<?= IMG ?>experience.png" alt="experience" class="fantastic-img experience-img">
						<?php else: ?>
							<img src="<?= IMG ?>fantastic.png" alt="fantastic" class="fantastic-img">
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<div class="<?= $slider_img ? 'col-xl-5 col-lg-6 col-12' : 'col-12'; ?> slider-col-content">
					<div class="base-slider" dir="rtl">
						<?php foreach ($args['content'] as $content) : ?>
							<div class="slider-base-item">
								<div class="base-output slider-output">
									<?= $content['content']; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
