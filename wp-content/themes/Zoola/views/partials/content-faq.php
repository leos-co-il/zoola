<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title">
						<?= (isset($args['block_text']) && $args['block_text']) ? $args['block_text'] : 'שאלתם? ענינו!'; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<span class="faq-body-title"><?= $item['faq_question']; ?></span>
										<img src="<?= ICONS ?>arrow-top.png" class="arrow-bottom faq-arrow" alt="arrow-top">
										<img src="<?= ICONS ?>arrow-bottom.png" class="arrow-top faq-arrow" alt="arrow-bottom">
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item answer-body"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="base-output slider-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
