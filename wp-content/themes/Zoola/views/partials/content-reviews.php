<?php if (isset($args['reviews']) && $args['reviews']) : ?>
	<section class="reviews-part my-3">
		<h2 class="happy-text">Happy Family</h2>
		<div class="reviews-block arrows-slider arrows-slider-base" <?php if (isset($args['img']) && $args['img']) : ?>
			style="background-image: url('<?= $args['img']['url']; ?>')"
		<?php endif; ?>>
			<div class="reviews-block-over"></div>
			<div class="container container-revs">
				<div class="row justify-content-end">
					<div class="col-lg-9 col-12">
						<div class="base-slider" dir="rtl">
							<?php foreach ($args['reviews'] as $x => $review) : ?>
								<div class="review-slide">
									<div class="review-item">
										<span class="quote quote-top">&rdquo;</span>
										<span class="quote quote-bottom">&ldquo;</span>
										<div class="rev-pop-trigger">
											<div class="hidden-review">
												<div class="base-output slider-output">
													<?= $review['review_text']; ?>
												</div>
											</div>
										</div>
										<div class="rev-content">
											<h3 class="base-block-title"><?= $review['review_name']; ?></h3>
											<p class="base-text text-center">
												<?= text_preview($review['review_text'], '30'); ?>
											</p>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
			 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="fas fa-times"></i>
					</button>
					<div class="modal-body" id="reviews-pop-wrapper"></div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
