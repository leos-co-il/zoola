<?php if (isset($args['cat']) && $args['cat']) : ?>
	<div class="col-lg-4 col-sm-6 col-12 cat-col">
		<div class="cat-item-img" <?php $thumbnail_id = get_term_meta( $args['cat']->term_id, 'thumbnail_id', true );
		$image = wp_get_attachment_url( $thumbnail_id );
		if ($image) : ?>
			style="background-image: url('<?= $image; ?>')"
		<?php endif; ?>>
			<span class="overlay-cat">
			</span>
			<span class="cat-item-info">
				<a class="base-title" href="<?= $link = get_category_link($args['cat']); ?>">
					<?= $args['cat']->name; ?>
				</a>
				<span class="cat-item-info-part">
					<span class="d-flex flex-column justify-content-start align-items-start">
						<span class="base-output">
							<?= text_preview(category_description($args['cat']), '20'); ?>
						</span>
						<a href="<?= $link; ?>" class="cat-link">
							לקטגוריה
						</a>
					</span>
				</span>
			</span>
		</div>
	</div>
<?php endif; ?>
