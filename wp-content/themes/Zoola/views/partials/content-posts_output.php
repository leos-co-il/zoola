<?php if ((isset($args['posts']) && $args['posts']) || (isset($args['link']) && $args['link'])) : ?>
	<section class="posts-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-8 col-12">
					<h2 class="base-block-title">
						<?= (isset($args['title']) && $args['title']) ? $args['title'] : ''; ?>
					</h2>
					<p class="block-text">
						<?= (isset($args['text']) && $args['text']) ? $args['text'] : ''; ?>
					</p>
				</div>
			</div>
			<?php if (isset($args['posts']) && $args['posts']) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($args['posts'] as $post) {
						get_template_part('views/partials/card', 'post', [
							'post' => $post,
						]);
					} ?>
				</div>
			<?php endif;
			if (isset($args['link']) && $args['link']) : ?>
				<div class="row justify-content-end mt-3">
					<div class="col-auto">
						<a href="<?= $args['link']['url']; ?>" class="block-link">
							<?= (isset($args['link']['title']) && $args['link']['title'])
									? $args['link']['title'] : 'לכל המאמרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
