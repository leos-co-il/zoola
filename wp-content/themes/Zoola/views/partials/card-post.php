<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-4 col-md-6 col-sm-10 col-12 mb-4 col-post">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-img"<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif; ?> href="<?= $link; ?>">
			</a>
			<div class="post-card-content">
				<div>
					<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
					<p class="block-text">
						<?= text_preview($args['post']->post_content, 15); ?>
					</p>
				</div>
				<a class="post-link" href="<?= $link; ?>">
					קרא עוד
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
