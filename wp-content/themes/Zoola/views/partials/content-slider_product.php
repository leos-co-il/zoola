<?php

if(!isset($args['products']))
	return;

?>

<section class="product-slider-block">
	<div class="container-fluid">
		<?php if (isset($args['title']) && $args['title']) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="base-title text-center">
						<?= $args['title']; ?>
					</h2>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center">
			<div class="col-md-10 col-11 arrows-slider">
				<div class="product-slider" dir="rtl">
					<?php

					foreach ($args['products'] as $_p){
						echo '<div class="slide-item p-2">';
						setup_postdata($GLOBALS['post'] =& $_p);
						wc_get_template_part( 'content', 'product' );
						echo '</div>';
					}
					wp_reset_postdata();

					?>
				</div>
			</div>
		</div>
	</div>
</section>
