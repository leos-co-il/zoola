<?php
$f_title_base = (isset($args['title']) && $args['title']) ? $args['title'] : 'חדר ארונות מותאם בשבילכם';
$f_img= (isset($args['img']) && $args['img']) ? $args['img'] : '';
?>
<div class="repeat-form-block" <?php if ($f_img) : ?>
	style="background-image: url('<?= $f_img['url']; ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h2 class="block-title"><?= $f_title_base; ?></h2>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-xl-5 col-lg-7 col-md-9 col-12">
				<?php getForm('8'); ?>
			</div>
		</div>
	</div>
</div>
