<?php if (isset($args['product'])) : $prod_item = $args['product'];
	$product = wc_get_product($prod_item);  ?>
	<div class="col-lg-4 col-sm-6 col-12 mb-3">
		<div class="product-small-card more-prod" data-id="<?= $product->get_id(); ?>">
			<div class="top">
				<?php if ($new = get_field('new', $product->get_id())) : ?>
					<span class="new-item">חדש</span>
				<?php endif;
				/**
				 * Hook: woocommerce_before_shop_loop_item.
				 *
				 * @hooked woocommerce_template_loop_product_link_open - 10
				 */
				do_action( 'woocommerce_before_shop_loop_item' );

				/**
				 * Hook: woocommerce_before_shop_loop_item_title.
				 *
				 * @hooked woocommerce_show_product_loop_sale_flash - 10
				 * @hooked woocommerce_template_loop_product_thumbnail - 10
				 */

				do_action( 'woocommerce_before_shop_loop_item_title' );

				?>
				<span class="top-overlay"></span>
			</div>
			<div class="bottom">
				<?php

				/**
				 * Hook: woocommerce_shop_loop_item_title.
				 *
				 * @hooked woocommerce_template_loop_product_title - 10
				 */
				do_action( 'woocommerce_shop_loop_item_title' );

				/**
				 * Hook: woocommerce_after_shop_loop_item_title.
				 *
				 * @hooked woocommerce_template_loop_rating - 5
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item_title' );

				/**
				 * Hook: woocommerce_after_shop_loop_item.
				 *
				 * @hooked woocommerce_template_loop_product_link_close - 5
				 * @hooked woocommerce_template_loop_add_to_cart - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item' );
				?>
			</div>
		</div>
	</div>
<?php endif; ?>
