<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 9,
	'post_type' => 'post',
	'suppress_filters' => false,
]);
$published_posts = count(get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
]))
?>

<article class="article-page-body page-body">
	<div class="title-wrap">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-auto">
					<h1 class="block-title">
						<?php the_title(); ?>
					</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="posts-page">
		<?php if ($posts->have_posts()) : ?>
			<div class="posts-output">
				<div class="container">
					<div class="row justify-content-center align-items-stretch put-here-posts">
						<?php foreach ($posts->posts as $i => $post) : ?>
							<?php get_template_part('views/partials/card', 'post', [
								'post' => $post,
							]); ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($published_posts && $published_posts > 9) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts">
						טען עוד
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['slider_img'],
					'content' => $fields['single_slider_seo'],
					'experience' => true
			]);
}
get_footer(); ?>
