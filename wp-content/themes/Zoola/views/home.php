<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>


<div class="container">
    <div class="row">
        <div class="col-12">

        </div>
    </div>
</div>
<?php if ($fields['main_slider']) : ?>
	<section class="main-block-home">
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['main_slider'] as $slide) : ?>
				<div class="top-archive top-main" <?php if ($slide['home_image']) : ?>
					style="background-image: url('<?= $slide['home_image']['url']; ?>')"<?php endif; ?>>
					<div class="archive-overlay-back top-main-overlay">
						<span class="vertical-text">Furniture</span>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-xl-6 col-lg-7 col-md-8 col-12">
								<h2 class="home-slide-title mb-3"><?= $slide['home_title']; ?></h2>
								<?php if ($slide['home_main_text']) : ?>
									<div class="base-output slider-output about-output main-output">
										<?= $slide['home_main_text']; ?>
									</div>
								<?php endif;
								if ($slide['home_white_text']) : ?>
									<p class="home-white-text"><?= $slide['home_white_text']; ?></p>
								<?php endif;
								if ($slide['home_link']) : ?>
									<div class="row justify-content-end mt-3">
										<div class="col-auto">
											<a href="<?= $slide['home_link']['url']; ?>" class="cat-link main-home-link">
												<?= (isset($slide['home_link']['title']) && $slide['home_link']['title'])
														? $slide['home_link']['title'] : 'לקטלוג המלא'; ?>
											</a>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif;
if ($fields['home_why_item']) : ?>
	<section class="why-us-block">
		<div class="container">
			<div class="row align-items-stretch justify-content-center">
				<div class="col-lg-6 col-12">
					<div class="tab-content">
						<?php foreach ($fields['home_why_item'] as $num => $item) : ?>
							<div class="tab-pane fade <?= ( $num === 0) ? 'show active' : ''; ?>" id="faq-<?= $num; ?>" role="tabpanel" aria-labelledby="<?= $num; ?>-tab">
								<div class="inside-tab">
									<div class="base-output slider-output about-output main-output">
										<?= $item['why_text']; ?>
									</div>
									<?php if ($item['why_link']) : ?>
										<div class="row justify-content-start mt-3">
											<div class="col-auto">
												<a href="<?= $item['why_link']['url']; ?>" class="cat-link about-home-link">
													<?= (isset($item['why_link']['title']) && $item['why_link']['title'])
															? $item['why_link']['title'] : 'עוד עלינו'; ?>
												</a>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="col-lg-6 col-12 d-flex flex-column justify-content-between tabs-line-order">
					<h3 class="base-title"><?= $fields['home_why_title']; ?></h3>
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<?php foreach ($fields['home_why_item'] as $num => $item) : ?>
							<li class="nav-item">
								<a class="nav-link <?= ( $num === 0) ? 'active' : ''; ?>" id="<?= $num; ?>-tab" data-toggle="tab" href="#faq-<?= $num; ?>" role="tab"
								   aria-controls="<?= $num; ?>" aria-selected="true">
									<div class="benefit-col">
										<?php if ($item['why_icon']) : ?>
											<div class="about-benefit-icon-wrap">
												<img src="<?= $item['why_icon']['url']; ?>" alt="benefit">
											</div>
										<?php endif;
										if ($item['why_title']) : ?>
											<h4 class="benefit-title mb-0"><?= $item['why_title']; ?></h4>
										<?php endif; ?>
									</div>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if (($fields['two_six_choice'] === '6') && $fields['home_cats']) :
	$chunks = array_chunk($fields['home_cats'], 3); ?>
	<section class="three-cats-block">
		<div class="container">
			<div class="row">
				<?php foreach ($chunks as $y => $chunk) : ?>
					<div class="col-lg-6 col-chunks">
						<div class="row justify-content-center align-items-stretch row-cats-layout">
							<?php foreach ($chunk as $x => $cat) : ?>
								<div class="cat-col">
									<div class="cat-item-img" <?php $thumbnail_id = get_term_meta($cat->term_id, 'thumbnail_id', true );
									$image = wp_get_attachment_url( $thumbnail_id );
									if ($image) : ?>
										style="background-image: url('<?= $image; ?>')"
									<?php endif; ?>>
										<span class="overlay-cat">
										</span>
										<span class="cat-item-info">
											<a class="base-title" href="<?= $link = get_category_link($cat); ?>">
												<?= $cat->name; ?>
											</a>
											<span class="cat-item-info-part">
												<span class="d-flex flex-column justify-content-start align-items-start">
													<span class="base-output">
														<?= text_preview(category_description($cat), '10'); ?>
													</span>
													<a href="<?= $link; ?>" class="cat-link">
														לקטגוריה
													</a>
												</span>
											</span>
										</span>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php else:
	if ($fields['home_cats']) : ?>
	<section class="two-cats my-4">
		<div class="container">
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['home_cats'] as $i => $cat) : ?>
					<div class="col-md-6 cat-col">
						<div class="cat-item-img" <?php $thumbnail_id = get_term_meta($cat->term_id, 'thumbnail_id', true );
						$image = wp_get_attachment_url( $thumbnail_id );
						if ($image) : ?>
							style="background-image: url('<?= $image; ?>')"
						<?php endif; ?>>
										<span class="overlay-cat">
										</span>
							<span class="cat-item-info">
											<a class="base-title" href="<?= $link = get_category_link($cat); ?>">
												<?= $cat->name; ?>
											</a>
											<span class="cat-item-info-part">
												<span class="d-flex flex-column justify-content-start align-items-start">
													<span class="base-output">
														<?= text_preview(category_description($cat), '10'); ?>
													</span>
													<a href="<?= $link; ?>" class="cat-link">
														לקטגוריה
													</a>
												</span>
											</span>
										</span>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; endif;
if ($fields['review_item']) {
	get_template_part('views/partials/content', 'reviews', [
			'img' => $fields['reviews_img'],
			'reviews' => $fields['review_item']
	]);
}
if ($fields['home_prod_slider']) {
	get_template_part('views/partials/content', 'slider_product',
			[
					'products' => $fields['home_prod_slider'],
					'title' => $fields['home_prod_slider_title'] ? $fields['home_prod_slider_title'] : 'בין המוצרים שלנו',
			]);
}
if ($fields['home_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['home_slider_img'],
					'content' => $fields['home_slider_seo'],
					'experience' => true
			]);
}
get_template_part('views/partials/repeat', 'form', [
		'img' => $fields['h_form_back'],
		'title' => $fields['h_form_title']
]);
if ($fields['h_posts'] || $fields['h_posts_link']) {
	get_template_part('views/partials/content', 'posts_output',
			[
					'title' => $fields['h_posts_title'] ? $fields['h_posts_title'] : 'קראו איתנו',
					'text' => $fields['h_posts_text'],
					'posts' => $fields['h_posts'],
					'link' => $fields['h_posts_link'],
			]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['slider_img'],
					'content' => $fields['single_slider_seo'],
					'experience' => false
			]);
}
get_footer(); ?>
