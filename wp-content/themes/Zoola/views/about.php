<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>

<article class="article-page-body page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-lg-10 col-12 d-flex flex-column align-items-center">
				<h1 class="block-title"><?php the_title(); ?></h1>
				<div class="base-output slider-output about-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['about_benefit']) : ?>
		<div class="repeating-about">
			<?php foreach ($fields['about_benefit'] as $content) : ?>
				<div class="base-part-block">
					<?php if ((isset($content['benefit_img']) && $content['benefit_img'])) : ?>
						<div class="part-image-wrap wow fadeInUp
						<?= !(isset($content['benefit_text']) && $content['benefit_text']) ? 'part-img-max-wrap' : ''; ?>">
							<img src="<?= $content['benefit_img']['url']; ?>" alt="image" class="slider-image">
						</div>
					<?php endif;
					if (isset($content['benefit_text']) && $content['benefit_text']) : ?>
						<div class="<?php echo !(isset($content['benefit_img']) && $content['benefit_img']) ? 'part-content-max-wrap' :
								'part-content-wrap'; ?> wow fadeInDown">
							<div class="part-text-wrap">
								<?php if ($content['benefit_icon']) : ?>
									<div class="about-benefit-icon-wrap">
										<img src="<?= $content['benefit_icon']['url']; ?>" alt="benefit">
									</div>
								<?php endif;
								if ($content['benefit_title']) : ?>
									<h4 class="benefit-title"><?= $content['benefit_title']; ?></h4>
								<?php endif; ?>
								<div class="base-output slider-output"><?= $content['benefit_text']; ?></div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</article>
<?php
if ($fields['review_item']) {
	get_template_part('views/partials/content', 'reviews', [
			'img' => $fields['reviews_img'],
			'reviews' => $fields['review_item']
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'block_text' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => $fields['slider_img'],
					'content' => $fields['single_slider_seo'],
					'experience' => false
			]);
}
get_footer(); ?>
