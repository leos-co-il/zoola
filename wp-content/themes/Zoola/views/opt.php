<?php
/*
Template Name: סיטונאות
*/

get_header();
$fields = get_fields();
$f_img = $fields['opt_form_img'] ? $fields['opt_form_img']['url'] : (has_post_thumbnail() ? postThumb() : '');
?>

<article class="article-page-body page-body opt-page">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-lg-10 col-12 d-flex flex-column align-items-center">
				<h1 class="block-title"><?php the_title(); ?></h1>
				<div class="base-output slider-output about-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['opt_benefits'] || $fields['opt_gallery']) : ?>
		<div class="container">
			<div class="row">
				<?php if ($fields['opt_gallery']) : ?>
					<div class="<?= $fields['opt_benefits'] ? 'col-lg-6 col-12' : 'col-12'?> gallery-slider-wrap">
						<div class="gallery-slider" dir="rtl">
							<?php if(has_post_thumbnail()): ?>
								<div class="p-2">
									<a class="big-slider-item" style="background-image: url('<?= postThumb(); ?>')"
									   href="<?= postThumb(); ?>" data-lightbox="images"></a>
								</div>
							<?php endif;
							foreach ($fields['opt_gallery'] as $img): ?>
								<div class="p-2">
									<a class="big-slider-item" style="background-image: url('<?= $img['url']; ?>')"
									   href="<?= $img['url']; ?>" data-lightbox="images">
									</a>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="thumbs" dir="rtl">
							<?php if(has_post_thumbnail()): ?>
								<div class="p-2">
									<a class="thumb-item" style="background-image: url('<?= postThumb(); ?>')"
									href="<?= postThumb(); ?>" data-lightbox="images-small"></a>
								</div>
							<?php endif;
							foreach ($fields['opt_gallery'] as $img): ?>
								<div class="p-2">
									<a class="thumb-item" style="background-image: url('<?= $img['url']; ?>')"
									   href="<?= $img['url']; ?>" data-lightbox="images-small">
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif;
				if ($fields['opt_benefits']) : ?>
					<div class="<?= $fields['opt_gallery'] ? 'col-lg-6 col-12': 'col-12'; ?>">
						<?php foreach ($fields['opt_benefits'] as $content) : ?>
							<div class="benefit-col">
								<?php if ($content['opt_ben_icon']) : ?>
									<div class="about-benefit-icon-wrap">
										<img src="<?= $content['opt_ben_icon']['url']; ?>" alt="benefit">
									</div>
								<?php endif;
								if ($content['opt_ben_title']) : ?>
									<h4 class="benefit-title mb-0"><?= $content['opt_ben_title']; ?></h4>
								<?php endif; ?>
								<p class="base-text-500"><?= $content['opt_ben_text']; ?></p>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
</article>
<section class="reviews-part my-3 opt-form-part">
	<h2 class="happy-text">Happy Family</h2>
	<div class="reviews-block arrows-slider arrows-slider-base" <?php if ($f_img) : ?>
		style="background-image: url('<?= $f_img; ?>')"
	<?php endif; ?>>
		<div class="reviews-block-over"></div>
		<div class="container container-revs">
			<div class="row justify-content-xl-end justify-content-center">
				<div class="col-xl-8 col-md-10 col-12">
					<?php if ($fields['opt_form_title']) : ?>
						<h3 class="opt-form-title"><?= $fields['opt_form_title']; ?></h3>
					<?php endif;
					if ($fields['opt_form_text']) : ?>
						<p class="contact-form-title text-right"><?= $fields['opt_form_text']; ?></p>
					<?php endif;
					getForm('7'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<div class="modal-body" id="reviews-pop-wrapper"></div>
			</div>
		</div>
	</div>
</section>
<?php
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'block_text' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>
