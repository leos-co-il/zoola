<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();
$cats = get_terms([
	'taxonomy' => 'product_cat',
	'hide_empty' => false,
])
?>

<article class="page-body">
	<div class="container">
		<div class="row justify-content-center align-items-center">
			<div class="col-auto">
				<h1 class="block-title">
					<?php the_title(); ?>
				</h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($cats) : ?>
		<div class="container">
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($cats as $i => $cat) : ?>
					<?php get_template_part('views/partials/card', 'category', [
						'cat' => $cat,
					]); ?>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_template_part('views/partials/repeat', 'form', [
	'img' => $fields['cats_form_back'],
	'title' => $fields['cats_form_title']
]);
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $fields['single_slider_seo'],
			'experience' => false
		]);
}
get_footer(); ?>
